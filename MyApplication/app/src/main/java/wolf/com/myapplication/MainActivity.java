package wolf.com.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText txtprimero, txtsegundo, txtoperacion;
    Button btnoperacion;

    Integer valor1;
    Integer valor2;
    Integer resultado;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtprimero= findViewById(R.id.txtprimero);
        txtsegundo= findViewById(R.id.txtsegundo);
        txtoperacion= findViewById(R.id.txtresultado);
        btnoperacion= findViewById(R.id.btnoperacion);



        btnoperacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                valor1= Integer.valueOf(txtprimero.getText().toString());
                valor2= Integer.valueOf(txtsegundo.getText().toString());

                resultado = valor1 + valor2 ;
                txtoperacion.setText(resultado.toString());



            }
        });



    }
}
