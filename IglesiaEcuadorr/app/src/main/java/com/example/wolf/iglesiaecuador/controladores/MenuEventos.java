package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.wolf.iglesiaecuador.R;


public class MenuEventos extends AppCompatActivity {

    Button crear, eventos, revisar, cumple;

    Intent siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_eventos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        crear = (Button)findViewById(R.id.btn_crear);
        eventos = (Button)findViewById(R.id.btn_eventos);
        revisar = (Button)findViewById(R.id.btn_revisar);
        cumple = (Button)findViewById(R.id.btn_cumple);


        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(MenuEventos.this, CrearEvento.class);
                startActivity(siguiente);

            }
        });

        eventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(MenuEventos.this, Eventos.class);
                startActivity(siguiente);

            }
        });

        revisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(MenuEventos.this, eventosRechazados.class);
                startActivity(siguiente);

            }
        });

        cumple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(MenuEventos.this, Cumple.class);
                startActivity(siguiente);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}
