package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

public class Ingresar extends AppCompatActivity {

    String IP = "https://ieanjesusoficial.org";
    String GETID = IP + "/web/ApiApp/api/iglesia/query_login.php";

    ObtenerWebServiceID hiloconexion2;

    Button ingresar;
    Button facial;

    EditText email;
    EditText clave;

    String varEmail;
    String varClave;

    Integer seconds = 0;

    public static String globalPreferenceName = "com.haangnd.profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar);

        Calendar c = Calendar.getInstance();
        seconds = c.get(Calendar.MINUTE);

        //mensaje(String.valueOf(seconds));

        ingresar = (Button)findViewById(R.id.btn_ingresar);
        facial = (Button)findViewById(R.id.btn_facial);

        email = (EditText)findViewById(R.id.et_email);
        clave = (EditText)findViewById(R.id.et_clave);

        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                varEmail = email.getText().toString();
                varClave = clave.getText().toString();

                    hiloconexion2 = new ObtenerWebServiceID();
                    String cadena = GETID + "?telefono=" + varEmail + "&clave=" + varClave;
                    //mensaje(cadena);
                    Log.d(cadena, " Boton");
                    hiloconexion2.execute(cadena,"1");


            }
        });

        facial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(Ingresar.this, Invitados.class);
                startActivity(siguiente);

            }
        });

    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    public class ObtenerWebServiceID extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("alumno"); //estado es el nombre del campo en el JSON

                            //Log.d(alumnosJSON.getJSONObject(0).getString("nombre_iglesia"),"esto es lo q trajo la parte de afuera de la iglesia");

                            Intent siguiente = new Intent(Ingresar.this, MainActivity.class);

                            SharedPreferences.Editor editor = getSharedPreferences(globalPreferenceName, MODE_PRIVATE).edit();

                            editor.putString("id",alumnosJSON.getJSONObject(0).getString("id"));
                            editor.putString("id_perfil",alumnosJSON.getJSONObject(0).getString("id_perfil"));
                            editor.putString("id_congregacion",alumnosJSON.getJSONObject(0).getString("id_congregacion"));
                            editor.putString("foto", IP + alumnosJSON.getJSONObject(0).getString("foto_usuario"));
                            editor.putString("nombre",alumnosJSON.getJSONObject(0).getString("nombre_completo"));
                            editor.putString("not",String.valueOf(seconds));

                            editor.commit();

                            startActivity(siguiente);



                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Usuario o clave incorrectos";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    //hilos para activar votacion

}
