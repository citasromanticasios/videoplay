package com.example.wolf.iglesiaecuador.controladores;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class RevisionLibros extends AppCompatActivity {

    String IP = "https://ieanjesusoficial.org/";
    String GET = IP + "web/ApiApp/api/iglesia/query_allCongre.php";
    String GETID = IP + "/web/ApiApp/api/iglesia/query_allIdIglesias2.php";
    String UPDATE = IP + "web/ApiApp/api/iglesia/query_insert_revision.php";

    ObtenerWebService hiloconexion;
    ObtenerWebServiceID hiloconexion2;
    ObtenerWebServiceInsert hiloconexion3;

    Spinner mes, templo, secretaria, tesoreria;

    String[] meses={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

    String[] validar = {"Si","No"};

    final ArrayList<String> iglesia = new ArrayList<String>();
    final ArrayList<String> ruta2= new ArrayList<String>();
    final ArrayList<String> titulo2 = new ArrayList<String>();
    final ArrayList<String> descricion2= new ArrayList<String>();

    Integer contador = 0;
    Integer tiempo = 3000;

    String id;
    String titulo;
    String descripcion;
    String ruta;

    ArrayList<Datos> arraydatos = new ArrayList<Datos>();
    Datos datos;
    AdapterDatoss adapter;

    ListView lista;

    EditText buscar;

    String bus = "";

    final ArrayList<String> data_cola_1 = new ArrayList<String>();
    final ArrayList<String> data_cola_2= new ArrayList<String>();
    final ArrayList<String> data_cola_3 = new ArrayList<String>();
    final ArrayList<String> data_cola_4= new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revision_libros);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mes = (Spinner)findViewById(R.id.cmb_mes);
        templo = (Spinner)findViewById(R.id.cmb_reviso);
        secretaria = (Spinner)findViewById(R.id.cmd_secretaria);
        tesoreria = (Spinner)findViewById(R.id.cmd_tesoreria);

        buscar = (EditText)findViewById(R.id.txt_buscar);

        if (isOnlineNet()){

            hiloconexion = new ObtenerWebService();
            hiloconexion.execute(GET,"1");
            temporizador();

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
        }

        mes.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, meses));
        secretaria.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, validar));
        tesoreria.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, validar));

        templo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                Toast.makeText(adapterView.getContext(),
                        (String) adapterView.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {    }
        });

        Button fab = (Button) findViewById(R.id.btn_buscar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOnlineNet()){

                    bus = buscar.getText().toString();
                    Log.d(bus, " Boton");
                    hiloconexion2 = new ObtenerWebServiceID();
                    String cadena = GETID + "?texto=" + buscar.getText().toString();
                    Log.d(cadena, " Boton");
                    hiloconexion2.execute(cadena,"1");
                    tiempo = 3000;
                    contador = 0;
                    updateListID();

                }else{
                    mensaje("Hermano, lo sentimos no tiene acceso a internet.");
                }

            }
        });

        FloatingActionButton enviar = (FloatingActionButton) findViewById(R.id.fab);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPreferences = getSharedPreferences(Ingresar.globalPreferenceName, MODE_PRIVATE);

                String id = sharedPreferences.getString("id","No Id");

                if (isOnlineNet()){

                    hilos(id,mes.getSelectedItem().toString(), String.valueOf(templo.getFirstVisiblePosition()), secretaria.getSelectedItem().toString(), tesoreria.getSelectedItem().toString());

                    mensaje("Guardado exitosamente");

                }else{



                }


            }
        });
    }

    public void temporizador(){

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();



    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private void updateList() {

        if (contador == 0){

            //prueba.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, iglesia));

        }else if (contador == 1){

            templo.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, iglesia));

            mensaje("Cargando");

        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;

    }

    private void updateListID() {

        if (contador == 0){

            //prueba.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, iglesia));

        }else if (contador == 1){

            templo.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, iglesia));

            //mensaje("Cargando");

        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;
        //lo aplico

    }

    public class ObtenerWebService extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombre_iglesia"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                devuelve = "Cargando";

                                //Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(i).getString("nombre_iglesia"),"esto es lo q trajo el estado");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                iglesia.add(alumnosJSON.getJSONObject(i).getString("nombre_iglesia"));

                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            String mensaje;

            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerWebServiceID extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombre_iglesia"),"esto es lo q trajo la parte de afuera de la iglesia");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            iglesia.clear();

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                devuelve = "Cargando";

                                //Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(i).getString("nombre_iglesia"),"esto es lo q la iglesia");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                iglesia.add(alumnosJSON.getJSONObject(i).getString("nombre_iglesia"));

                                titulo = alumnosJSON.getJSONObject(i).getString("nombre_iglesia");

                                datos = new Datos(ruta, titulo, descripcion);

                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter = new AdapterDatoss(RevisionLibros.this, arraydatos);

                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerWebServiceInsert extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="4"){  //update

                try {
                    HttpURLConnection urlConn;

                    DataOutputStream printout;
                    DataInputStream input;
                    url = new URL(cadena);
                    urlConn = (HttpURLConnection) url.openConnection();
                    urlConn.setDoInput(true);
                    urlConn.setDoOutput(true);
                    urlConn.setUseCaches(false);
                    urlConn.setRequestProperty("Content-Type", "application/json");
                    urlConn.setRequestProperty("Accept", "application/json");
                    urlConn.connect();
                    //creo el objeto json
                    JSONObject jsonparam = new JSONObject();
                    jsonparam.put("id_usuario", params[2]);
                    jsonparam.put("mes", params[3]);
                    jsonparam.put("iglesia", params[4]);
                    jsonparam.put("secretaria", params[5]);
                    jsonparam.put("tesoreria", params[6]);
                    //envio los parametros post.
                    OutputStream os = urlConn.getOutputStream();
                    BufferedWriter write = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    write.write(jsonparam.toString());
                    write.flush();
                    write.close();

                    int respuesta = urlConn.getResponseCode();

                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                        while ((line = br.readLine()) != null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());

                        String resultJSON = respuestaJSON.getString("estado");

                        if (resultJSON == "1"){ //alumno actualizado correctamente
                            devuelve = "Evento Creado";

                            Intent siguiente = new Intent(RevisionLibros.this, MainActivity.class);
                            startActivity(siguiente) ;

                        }else if(resultJSON == "2"){
                            devuelve = "No se creo el evento";
                            //mensaje("Falla al guardar");
                            Log.d(devuelve,"resultado");
                        }
 
                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public void hilos(String id_usuario, String mes, String iglesia, String secretaria, String tesoreria){

        hiloconexion3 = new ObtenerWebServiceInsert();
        hiloconexion3.execute(UPDATE,"4",id_usuario,mes,iglesia,secretaria,tesoreria);

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private void Carga_en_cola() {

        if (isOnlineNet()){

            Integer con = data_cola_1.size();

            for (int i = 0; i < data_cola_1.size(); i++) {

                //hilos(data_cola_1.get(i).toString(),data_cola_2.get(i).toString(), data_cola_3.get(i).toString(), data_cola_4.get(i).toString(), status);

                data_cola_1.remove(i);
                data_cola_2.remove(i);
                data_cola_3.remove(i);
                data_cola_4.remove(i);

                if (con == 0 ){

                    tiempo = 1000000;

                }

            }

        }else{


        }

    }



}
