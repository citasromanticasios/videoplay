package com.example.wolf.iglesiaecuador.controladores;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.wolf.iglesiaecuador.R;


/**
 * Created by reale on 13/07/2016.
 */
public class ViewPagerAdapter2 extends PagerAdapter {

        Activity activity;
        String[] images;
        String[] textos;
        LayoutInflater inflater;

        public ViewPagerAdapter2(Activity activity, String[] images, String[] textos) {
            this.activity = activity;
            this.images = images;
            this.textos = textos;

        }


        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            inflater = (LayoutInflater)activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.viewpager_item_web,container,false);

            WebView web;

            web = (WebView)itemView.findViewById(R.id.web);

            WebSettings webSettings = web.getSettings();
            webSettings.setJavaScriptEnabled(true);
            web.setWebViewClient(new WebViewClient());
            web.loadUrl(textos[position]);

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View)object);
        }
    }