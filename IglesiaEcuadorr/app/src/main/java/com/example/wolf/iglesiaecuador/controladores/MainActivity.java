package com.example.wolf.iglesiaecuador.controladores;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//import android.support.v7.app.NotificationCompat;

public class MainActivity extends AppCompatActivity {

    Intent siguiente;

    TextView bautizados;
    TextView bautizadosES;
    TextView jovenes;
    TextView comites;
    TextView membrecias;

    String IP = "https://ieanjesusoficial.org/";
    //String IP = "http://190.214.46.83/";
    String GETCountBautizados = IP + "web/ApiApp/api/iglesia/query_allCount.php?texto=";
    String GETCountBautizadosES = IP + "web/ApiApp/api/iglesia/query_allESCount.php?texto=";
    String GETCountComites = IP + "web/ApiApp/api/iglesia/query_allComiteCount.php?texto=";
    String GETCountMembrecias = IP + "web/ApiApp/api/iglesia/query_allMembreciaCount.php?texto=";
    String GETCountJovenes = IP + "web/ApiApp/api/iglesia/query_allCountJovenes.php?texto=";
    String GETCountNoti = IP + "web/ApiApp/api/iglesia/query_allNotiCount.php?texto=";
    String GETCountCumple = IP + "web/ApiApp/api/iglesia/query_allCumpleCount.php?texto=";

    private String[] RUTAS = {

            "GETCountBautizados",
            "GETCountBautizadosES",
            "GETCountComites",
            "GETCountMembrecias",
            "GETCountJovenes"

    };

    String GETActVotar = IP + "web/ApiApp/api/iglesia/query_allActVotar.php";
    String GETValidarVotar = IP + "web/ApiApp/api/iglesia/query_allValidarVotar.php?texto=";
    String INSERTVotar = IP + "web/ApiApp/api/iglesia/query_insertVotar.php";

    String GETNotificaiones = IP + "web/ApiApp/api/iglesia/query_allNotificaciones.php?texto=";

    ObtenerBautizados hiloconexion;
    ObtenerBautizadosES hiloconexion2;
    ObtenerComites hiloconexion3;
    ObtenerMembrecias hiloconexion4;
    ObtenerNotificaciones hiloconexion5;
    ObtenerCumple hiloconexion6;
    Obtener hiloconexion7;
    ObtenerJovenes hiloconexion8;
    ObtenerActVotar hiloconexion9;
    ObtenerValidarVotar hiloconexion10;
    ObtenerInsertVotar hiloconexion11;

    String countBautizos;
    String countBautizosES;
    String countJovenes;
    String countComites;
    String countMembrecias;

    final ArrayList<String> CONTADORES = new ArrayList<String>();

    String contenido;

    Integer countNotificaciones = 0;
    Integer countCumple = 0;
    Integer tiempo = 3000;
    Integer contador = 0;
    Integer con = 0;
    Integer conCumple = 0;
    Integer countRadio = 0;

    Integer not = 0;

    String id_perfil, id_congregacion, id_usuario;

    Integer seconds = 0;

    private Button boton, votar;

    NotificationManager nms;
    Notification notifications;
    NotificationCompat.Builder builders;
    NotificationCompat.Builder notificacion;
    RemoteViews contentViews;

    int notifyID=1;
    private String TAG="Error : ";
    String textoActual="";
    String hora="";

    private static final int idUnica = 51623;

    private String STREAM_URL ="http://62.210.141.243:8151/stream/1/";
    private MediaPlayer mPlayer;

    ViewPager viewPager;
    ViewPagerAdapter adapter;

    String mostrar;

    private String[] images = {

            "https://ieanjesusoficial.org/images/im1.png",
            "https://ieanjesusoficial.org/images/im2.png",
            "http://www.ieanjesus.org.ec/wp-content/uploads/2011/12/BANNER.jpg",
            "http://www.ieanjesus.org.ec/wp-content/uploads/2011/12/Todoloque1.jpg"

    };

    public static String globalPreferenceName = "com.haangnd.profile";

    ImageView totales, miembros, notificaciones, perfil, salir, fallas, tesoreria, eventos;

    RelativeLayout rellay_timeline, rellay_friends, rellay_chat, rellay_music,
            rellay_gallery, rellay_map, rellay_weather, rellay_settings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hilos(id_congregacion);

        //modal_votar();

        SharedPreferences sharedPreferences = getSharedPreferences(Ingresar.globalPreferenceName, MODE_PRIVATE);

        id_usuario = sharedPreferences.getString("id","Id perdido");
        //mostrar = sharedPreferences.getString("mostrar","0");
        id_perfil = sharedPreferences.getString("id_perfil","Id perdido");
        id_congregacion = sharedPreferences.getString("id_congregacion","Id perdido");
        not = Integer.valueOf(sharedPreferences.getString("not","Id perdido"));

        Calendar c = Calendar.getInstance();
        seconds = c.get(Calendar.MINUTE);

        notificacion = new NotificationCompat.Builder(this);
        notificacion.setAutoCancel(true);

        //isNetDisponible();
        //isOnlineNet();

        if (isOnlineNet()){

            hilos(id_congregacion);
            temporizador();

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
        }

        rellay_timeline = (RelativeLayout) findViewById(R.id.rellay_timeline);
        rellay_friends = findViewById(R.id.rellay_friends);
        rellay_chat = findViewById(R.id.rellay_chat);
        rellay_music = findViewById(R.id.rellay_music);
        rellay_gallery = findViewById(R.id.rellay_gallery);
        rellay_map = findViewById(R.id.rellay_map);
        rellay_weather = findViewById(R.id.rellay_weather);
        rellay_settings = findViewById(R.id.rellay_settings);

        rellay_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(MainActivity.this, Perfil.class);
                startActivity(siguiente) ;

            }
        });

        rellay_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(MainActivity.this, Totales.class);

                siguiente.putExtra("bautizados", countBautizos);
                siguiente.putExtra("es", countBautizosES);
                siguiente.putExtra("comites", countComites);
                siguiente.putExtra("jovenes", countJovenes);
                siguiente.putExtra("membrecia", countMembrecias);

                startActivity(siguiente);

            }
        });
        rellay_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(MainActivity.this, MenuEventos.class);
                startActivity(siguiente) ;

            }
        });
        rellay_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(MainActivity.this, Notificaciones.class);
                startActivity(siguiente) ;

            }
        });
        rellay_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(MainActivity.this, ReportarFallas.class);
                startActivity(siguiente) ;

            }
        });
        rellay_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(MainActivity.this, Invitados.class);
                startActivity(siguiente) ;

            }
        });
        rellay_weather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });
        rellay_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });


    }

    //Hilos de conexion para los contadores de registros

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private  void temporizador(){
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {

                    mensaje(e.toString());

                }
            }
        };

        t.start();
    }

    private void updateList() {

        hiloNoti(id_congregacion);

        if (contador == 0){

            consutaNotificaciones();
            con = countNotificaciones;


        }else{


            //tiempo = 10000;

            if (Integer.valueOf(countNotificaciones) > con){

                consutaNotificaciones();
                con = countNotificaciones;

            }

            if (Integer.valueOf(countCumple) > conCumple){

                consutaNotificaciones();
                conCumple = countCumple;

            }

            Log.d(countNotificaciones.toString(), "Contador de notificacione");
            Log.d(contador.toString(), "Contador de notificacione");

        }

        contador++;

    }

    public void hilos(String texto){

        hiloconexion = new ObtenerBautizados();
        hiloconexion.execute(GETCountBautizados + texto,"1");

        hiloconexion2 = new ObtenerBautizadosES();
        hiloconexion2.execute(GETCountBautizadosES + texto,"1");

        hiloconexion3 = new ObtenerComites();
        hiloconexion3.execute(GETCountComites + texto,"1");

        hiloconexion4 = new ObtenerMembrecias();
        hiloconexion4.execute(GETCountMembrecias + texto,"1");

        hiloconexion8 = new ObtenerJovenes();
        hiloconexion8.execute(GETCountJovenes + texto,"1");

        hiloconexion5 = new ObtenerNotificaciones();
        hiloconexion5.execute(GETCountNoti + texto,"1");

        hiloconexion6 = new ObtenerCumple();
        hiloconexion6.execute(GETCountCumple + texto,"1");

        if (not == seconds){
            hiloconexion7 = new Obtener();
            hiloconexion7.execute(GETNotificaiones + id_perfil,"1");
        }else{

        }


        //mensaje(GETNotificaiones + id_perfil);

    }

    public void hiloNoti(String valor){

        hiloconexion5 = new ObtenerNotificaciones();
        hiloconexion5.execute(GETCountNoti + valor,"1");


    }

    // hilos de bautizados

    public class ObtenerBautizados extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countBautizos = alumnosJSON.getJSONObject(0).getString("total");
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    //hilos para activar votacion

    public class ObtenerActVotar extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("estado"),"esto es lo q trajo el estado f4era");

                            String validar = alumnosJSON.getJSONObject(0).getString("estado");

                            if (validar.equals("1")){

                                votar.setVisibility(View.VISIBLE);

                            }

                        }
                        else if (resultJSON.equals("2")){

                            //devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerValidarVotar extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            votar.setVisibility(View.GONE);

                        }
                        else if (resultJSON.equals("2")){

                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);
            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerInsertVotar extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="4"){  //update

                try {
                    HttpURLConnection urlConn;

                    DataOutputStream printout;
                    DataInputStream input;
                    url = new URL(cadena);
                    urlConn = (HttpURLConnection) url.openConnection();
                    urlConn.setDoInput(true);
                    urlConn.setDoOutput(true);
                    urlConn.setUseCaches(false);
                    urlConn.setRequestProperty("Content-Type", "application/json");
                    urlConn.setRequestProperty("Accept", "application/json");
                    urlConn.connect();
                    //creo el objeto json
                    JSONObject jsonparam = new JSONObject();
                    jsonparam.put("id_usuario", params[2]);
                    jsonparam.put("respuesta", params[3]);
                    jsonparam.put("estado", params[4]);
                    //envio los parametros post.
                    OutputStream os = urlConn.getOutputStream();
                    BufferedWriter write = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    write.write(jsonparam.toString());
                    write.flush();
                    write.close();

                    int respuesta = urlConn.getResponseCode();

                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                        while ((line = br.readLine()) != null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());

                        String resultJSON = respuestaJSON.getString("estado");

                        if (resultJSON == "1"){ //alumno actualizado correctamente
                            devuelve = "Respuesta enviada";
                            Log.d(devuelve,"resultado");
                        }else if(resultJSON == "2"){
                            devuelve = "No se envio la respuesta";
                            Log.d(devuelve,"resultado");
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    //hilos de bautizados ES

    public class ObtenerBautizadosES extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countBautizosES = alumnosJSON.getJSONObject(0).getString("total");
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

//            Toast toast1 =
//                    Toast.makeText(getApplicationContext(),
//                            s, Toast.LENGTH_SHORT);
//
//            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerComites extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countComites = alumnosJSON.getJSONObject(0).getString("total");
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

//            Toast toast1 =
//                    Toast.makeText(getApplicationContext(),
//                            s, Toast.LENGTH_SHORT);
//
//            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    //hilos de membrecias

    public class ObtenerMembrecias extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countMembrecias = alumnosJSON.getJSONObject(0).getString("total");
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    //hilo de notificaciones

    public class ObtenerNotificaciones extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countNotificaciones = Integer.valueOf(alumnosJSON.getJSONObject(0).getString("total"));
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerCumple extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countCumple = Integer.valueOf(alumnosJSON.getJSONObject(0).getString("total"));
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class Obtener extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            //Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            contenido = alumnosJSON.getJSONObject(0).getString("mensaje");
                            creaNotificacion(alumnosJSON.getJSONObject(0).getString("mensaje"));
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public void notiCumple(){

        notificacion.setSmallIcon(R.mipmap.ic_launcher);
        notificacion.setTicker("Nuevas notificaciones");
        notificacion.setPriority(Notification.PRIORITY_HIGH);
        notificacion.setWhen(System.currentTimeMillis());
        notificacion.setContentTitle("IEANJESUS");
        notificacion.setContentText("Tienes " + countCumple + " Cumpleaños el dia de hoy");

        Intent intent = new Intent(MainActivity.this, Cumple.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        notificacion.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(idUnica,notificacion.build());

    }

    public void noti(){

        notificacion.setSmallIcon(R.mipmap.ic_launcher);
        notificacion.setTicker("Nuevas notificaciones");
        notificacion.setPriority(Notification.PRIORITY_HIGH);
        notificacion.setWhen(System.currentTimeMillis());
        notificacion.setContentTitle("IEANJESUS");
        notificacion.setContentText("Tienes " + countNotificaciones + " notificaciones nuevas");

        Intent intent = new Intent(MainActivity.this,Notificaciones.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        notificacion.setContentIntent(pendingIntent);


        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(idUnica,notificacion.build());

    }

    public void notificaciones(String texto){

        notificacion.setSmallIcon(R.mipmap.ic_launcher);
        notificacion.setTicker("Nuevas notificaciones");
        notificacion.setPriority(Notification.PRIORITY_HIGH);
        notificacion.setWhen(System.currentTimeMillis());
        notificacion.setContentTitle("IEANJESUS");
        notificacion.setContentText(texto);


        Intent intent = new Intent(MainActivity.this,MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        notificacion.setContentIntent(pendingIntent);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(idUnica,notificacion.build());

    }

    public void consutaNotificaciones(){

        if (countNotificaciones > 0){

            noti();

        }

        if (countCumple > 0){

            notiCumple();

        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private boolean isNetDisponible() {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo actNetInfo = connectivityManager.getActiveNetworkInfo();

        return (actNetInfo != null && actNetInfo.isConnected());
    }

    private void setNotification(String textoVista,String textoHora){
        contentViews.setTextViewText(R.id.textView,textoVista);
        contentViews.setTextViewText(R.id.txtHora,textoHora);

        notifications.contentView=contentViews;
    }

    private void setNotificationExpandida(String textoVista,String textoHora){
        if(Build.VERSION.SDK_INT>=16){
            RemoteViews expandedView=new RemoteViews(getPackageName(),R.layout.notificacion_expandida);
            expandedView.setTextViewText(R.id.txtMensajes,textoVista);
            expandedView.setTextViewText(R.id.txtHora,textoHora);

            notifications.bigContentView=expandedView;
        }
    }

    private void creaNotificacion(final String textoActualExpandido){
        builders= new NotificationCompat.Builder(this);

        Intent i=new Intent(this,MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent=PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        builders.setContentIntent(intent);

        builders.setTicker(getResources().getString(R.string.etiqueta_notificacion));
        builders.setSmallIcon(R.drawable.logo3);
        builders.setAutoCancel(true);

        notifications=builders.build();

        contentViews=new RemoteViews(getPackageName(),R.layout.notificacion);

        textoActual="IEANJESUS - ECUADOR Nueva notificacion";
        hora= DateFormat.getTimeInstance().format(new Date()).toString();
        setNotification(textoActual,hora);
        setNotificationExpandida(textoActual, hora);

        nms=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        nms.notify(notifyID,notifications);

        //Creamos un hilo
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {

                            hora=DateFormat.getTimeInstance().format(new Date()).toString();

                            String tex = textoActualExpandido;
                            setNotification(textoActual,hora);

                            setNotificationExpandida(tex,hora);

                            nms.notify(notifyID,notifications);

                            try{
                                Thread.sleep(2*1000);
                            }catch (InterruptedException e){
                                Log.d(TAG," sleep error");
                            }
                        }
                    }

        ).start();

    }

    public class ObtenerJovenes extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("total"),"esto es lo q trajo el estado f4era");

                            countJovenes = alumnosJSON.getJSONObject(0).getString("total");
                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public void hiloInsert(String id_usuario, String respuesta, String estado){

        hiloconexion11 = new ObtenerInsertVotar();
        hiloconexion11.execute(INSERTVotar,"4",id_usuario,respuesta,estado);

    }

    public void modal_votar(){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.modal_votacion, null);

        Button mSi = (Button) mView.findViewById(R.id.btn_si);
        Button mNo = (Button) mView.findViewById(R.id.btn_no);

        //final EditText pass = (EditText) mView.findViewById(R.id.txt_clave);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        mSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hiloInsert(id_usuario,"1","1");

                hiloconexion10 = new ObtenerValidarVotar();
                hiloconexion10.execute(GETValidarVotar,"1");

                votar.setVisibility(View.GONE);

                SharedPreferences.Editor editor = getSharedPreferences(globalPreferenceName, MODE_PRIVATE).edit();

                editor.putString("mostrar","1");

                editor.commit();


                dialog.dismiss();

            }
        });

        mNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hiloInsert(id_usuario,"0","1");
                dialog.dismiss();

            }
        });


        }

}
