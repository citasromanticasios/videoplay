package com.example.wolf.iglesiaecuador.controladores;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

public class CrearEvento extends AppCompatActivity implements View.OnClickListener {

    ObtenerWebService hiloconexion;

    //String IP = "http://190.214.46.83/";
    String IP = "https://ieanjesusoficial.org/";
    String UPDATE = IP + "web/ApiApp/api/iglesia/query_crearEvento.php";

    EditText titulo;
    EditText descripcion;

    EditText fin;
    EditText inicio;

    EditText mesini;
    EditText anoini;
    EditText timeini;

    EditText mesfin;
    EditText anofin;
    EditText timefin;

    DatePicker calendario;

    String inicio_completo;
    String fin_completo;
    String id;
    String id_congregacion;

    String status = "Ingresado";

    private  int dia,mes,ano,hora,minutos;

    final ArrayList<String> data_cola_1 = new ArrayList<String>();
    final ArrayList<String> data_cola_2= new ArrayList<String>();
    final ArrayList<String> data_cola_3 = new ArrayList<String>();
    final ArrayList<String> data_cola_4= new ArrayList<String>();

    Integer contador = 0;
    Integer tiempo = 3000;

    DatePicker datepicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_evento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPreferences = getSharedPreferences(Ingresar.globalPreferenceName, MODE_PRIVATE);

        id = sharedPreferences.getString("id","No Id");
        id_congregacion = sharedPreferences.getString("id_congregacion","Id congregacion perdido");

        titulo = (EditText)findViewById(R.id.et_titulo);
        descripcion = (EditText)findViewById(R.id.et_observacion);
        inicio = (EditText)findViewById(R.id.et_inicio);
        fin = (EditText)findViewById(R.id.et_fin);

        mesini = (EditText)findViewById(R.id.et_mesini);
        anoini = (EditText)findViewById(R.id.et_anoini);
        timeini = (EditText)findViewById(R.id.et_timeini);

        mesfin = (EditText)findViewById(R.id.et_mesfin);
        anofin = (EditText)findViewById(R.id.et_anofin);
        timefin = (EditText)findViewById(R.id.et_timefin);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("Campor:", titulo.getText().toString());
                Log.d("Campor:", descripcion.getText().toString());
                Log.d("Campor:", inicio.getText().toString());
                Log.d("Campor:", fin.getText().toString());

                inicio_completo = anoini.getText().toString() + "-" + mesini.getText().toString() + "-" + inicio.getText().toString() + " " + timeini.getText().toString() + ":00";
                fin_completo = anofin.getText().toString() + "-" + mesfin.getText().toString() + "-" + fin.getText().toString() + " " + timefin.getText().toString() + ":00";

                String desc = descripcion.getText().toString();

                if (desc.equals("")){

                    mensaje("Coloque una descripcion");

                }else {

                    if (isOnlineNet()){

                        hilos(titulo.getText().toString(),descripcion.getText().toString(), inicio_completo.toString(), fin_completo.toString(), status);

                    }else{

                        mensaje("Hermano, lo sentimos no tiene acceso a internet., se cargara la data al restablecer la conexion");

                        data_cola_1.add(titulo.getText().toString());
                        data_cola_2.add(descripcion.getText().toString());
                        data_cola_3.add(inicio_completo.toString());
                        data_cola_4.add(fin_completo.toString());


                        if (tiempo == 1000000){

                            tiempo = 3000;

                        }

                        temporizador();

                    }

                    titulo.setText("");
                    descripcion.setText("");


                }


            }
        });

        Button fab_c_ini = (Button) findViewById(R.id.fab_c_ini);
        Button fab_t_ini = (Button) findViewById(R.id.fab_t_ini);
        Button fab_c_fin = (Button) findViewById(R.id.fab_c_fin);
        Button fab_t_fin = (Button) findViewById(R.id.fab_t_fin);

        fab_c_ini.setOnClickListener(this);
        fab_t_ini.setOnClickListener(this);
        fab_c_fin.setOnClickListener(this);
        fab_t_fin.setOnClickListener(this);

    }

    public void hilos(String tit, String obs, String ini, String fn, String sta){

        hiloconexion = new ObtenerWebService();
        hiloconexion.execute(UPDATE,"4",tit,obs,ini,fn,sta, id, id_congregacion);

    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        Button b = (Button) view;
        switch(b.getId()) {
            case R.id.fab_c_ini:

                final Calendar c= Calendar.getInstance();
                dia=c.get(Calendar.DAY_OF_MONTH);
                mes=c.get(Calendar.MONTH);
                ano=c.get(Calendar.YEAR);

                final DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        inicio.setText(Integer.toString(dayOfMonth));
                        mesini.setText(Integer.toString(monthOfYear+1));
                        anoini.setText(Integer.toString(year));

                    }

                }
                        ,ano,mes,dia);


                datePickerDialog.show();

                break;
            case R.id.fab_t_ini:

                final Calendar d = Calendar.getInstance();
                hora=d.get(Calendar.HOUR_OF_DAY);
                minutos=d.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        timeini.setText(hourOfDay+":"+minute);
                    }
                },hora,minutos,false);
                timePickerDialog.show();

                break;
            case R.id.fab_c_fin:

                final Calendar e= Calendar.getInstance();
                dia=e.get(Calendar.DAY_OF_MONTH);
                mes=e.get(Calendar.MONTH);
                ano=e.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog2 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        fin.setText(Integer.toString(dayOfMonth));
                        mesfin.setText(Integer.toString(monthOfYear+1));
                        anofin.setText(Integer.toString(year));
                    }
                }
                        ,ano,mes,dia);
                datePickerDialog2.show();

                break;
            case R.id.fab_t_fin:

                final Calendar f = Calendar.getInstance();
                hora=f.get(Calendar.HOUR_OF_DAY);
                minutos=f.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog2 = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        timefin.setText(hourOfDay+":"+minute);
                    }
                },hora,minutos,false);
                timePickerDialog2.show();

                break;

        }
    }

    public class ObtenerWebService extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="4"){  //update

                try {
                    HttpURLConnection urlConn;

                    DataOutputStream printout;
                    DataInputStream input;
                    url = new URL(cadena);
                    urlConn = (HttpURLConnection) url.openConnection();
                    urlConn.setDoInput(true);
                    urlConn.setDoOutput(true);
                    urlConn.setUseCaches(false);
                    urlConn.setRequestProperty("Content-Type", "application/json");
                    urlConn.setRequestProperty("Accept", "application/json");
                    urlConn.connect();
                    //creo el objeto json
                    JSONObject jsonparam = new JSONObject();
                    jsonparam.put("title", params[2]);
                    jsonparam.put("observacion", params[3]);
                    jsonparam.put("start", params[4]);
                    jsonparam.put("end", params[5]);
                    jsonparam.put("estado", params[6]);
                    jsonparam.put("usuario", params[7]);
                    jsonparam.put("id_congregacion", params[8]);
                    //envio los parametros post.
                    OutputStream os = urlConn.getOutputStream();
                    BufferedWriter write = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    write.write(jsonparam.toString());
                    write.flush();
                    write.close();

                    int respuesta = urlConn.getResponseCode();

                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                        while ((line = br.readLine()) != null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());

                        String resultJSON = respuestaJSON.getString("estado");

                        if (resultJSON == "1"){ //alumno actualizado correctamente
                            devuelve = "Evento Creado";
                            Log.d(devuelve,"resultado");
                        }else if(resultJSON == "2"){
                            devuelve = "No se creo el evento";
                            Log.d(devuelve,"resultado");
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void temporizador(){

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Carga_en_cola();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();



    }

    private void Carga_en_cola() {

        if (isOnlineNet()){

            Integer con = data_cola_1.size();

            for (int i = 0; i < data_cola_1.size(); i++) {

                hilos(data_cola_1.get(i).toString(),data_cola_2.get(i).toString(), data_cola_3.get(i).toString(), data_cola_4.get(i).toString(), status);

                data_cola_1.remove(i);
                data_cola_2.remove(i);
                data_cola_3.remove(i);
                data_cola_4.remove(i);

                if (con == 0 ){

                    tiempo = 1000000;

                }

            }

        }else{


        }

    }



}
