package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.wolf.iglesiaecuador.R;

public class FotoIglesia extends AppCompatActivity {

    String numero;

    ImageView img;

    String foto;

    String ruta = "https://ieanjesusoficial.org/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto_iglesia);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        img = (ImageView) findViewById(R.id.img_iglesia);

        numero = getIntent().getStringExtra("numero");
        foto = ruta + getIntent().getStringExtra("imagen");

        Glide.with(this).load(foto).into(img);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(getApplicationContext(), numero, Toast.LENGTH_LONG).show();

                Intent siguiente = new Intent(Intent.ACTION_DIAL);
                siguiente.setData(Uri.parse("tel:" + numero));

                startActivity(siguiente);

            }
        });
    }

}
