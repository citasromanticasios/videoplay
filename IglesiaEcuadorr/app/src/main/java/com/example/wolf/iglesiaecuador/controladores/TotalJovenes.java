package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class TotalJovenes extends AppCompatActivity {

    String IP = "https://ieanjesusoficial.org/";
    String GET = IP + "web/ApiApp/api/iglesia/query_allJovenes.php?texto=";
    String INSERT = IP + "web/ApiApp/api/iglesia/query_insertAsistencia.php";

    String GETValidar = IP + "web/ApiApp/api/iglesia/query_allAsistenciasValidar.php?texto=";
    String validar = "&texto2=";

    String img = "http://190.214.46.83/ieanjesus_general/";

    String img2 = "http://www.redcda.org/wp-content/uploads/2014/10/13984593091381272676iconoperfilazul.png";

    String mensaje = "";

    ArrayList<Datos> arraydatos = new ArrayList<Datos>();
    Datos datos;
    AdapterDatoss adapter;

    ObtenerWebService hiloconexion;
    ObtenerWebServiceValidar hiloconexionValidar;
    ObtenerWebServiceInsert hiloconexionInsert;

    String id;
    String titulo;
    String descripcion;
    String ruta;

    Integer contador = 0;
    Integer tiempo = 3000;

    final ArrayList<String> m_id = new ArrayList<String>();
    final ArrayList<String> ruta2= new ArrayList<String>();
    final ArrayList<String> titulo2 = new ArrayList<String>();
    final ArrayList<String> descricion2= new ArrayList<String>();

    ListView lista;

    String id_congregacion = "125";
    String idd = "1";
    String id_perfil = "5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_jovenes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lista = (ListView)findViewById(R.id.lista);

        SharedPreferences sharedPreferences = getSharedPreferences(Ingresar.globalPreferenceName, MODE_PRIVATE);

        id_congregacion = sharedPreferences.getString("id_congregacion","Id perdido");
        idd = sharedPreferences.getString("id","No Id");
        id_perfil = sharedPreferences.getString("id_perfil","No Id");

        if (isOnlineNet()){

            hiloconexion = new ObtenerWebService();
            hiloconexion.execute(GET + id_congregacion,"1");
            temporizador();

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
            Log.d(ruta,"no tienes internet");
        }

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                modal(m_id.get(position).toString());

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(TotalJovenes.this, Asistencia.class);
                startActivity(siguiente);

            }
        });

        if (id_perfil.equals("6")||id_perfil.equals("5")){

        }else{
            fab.setVisibility(View.GONE);
        }

    }

    public void modal(final String ids){

        if (id_perfil.equals("6")||id_perfil.equals("5")){

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(TotalJovenes.this);
            View mView =getLayoutInflater().inflate(R.layout.dialog_login, null);

            Button mSi = (Button) mView.findViewById(R.id.btnSi);
            Button mNo = (Button) mView.findViewById(R.id.btnNo);

            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            dialog.show();

            mSi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    hiloconexionValidar = new ObtenerWebServiceValidar();
                    hiloconexionValidar.execute(GETValidar + id_congregacion + validar + ids,"1",ids);



                    dialog.dismiss();

                }
            });

            mNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    hiloconexionValidar = new ObtenerWebServiceValidar();
                    hiloconexionValidar.execute(GETValidar + id_congregacion + validar + ids,"1",ids);

                    dialog.dismiss();

                }
            });

        }

    }

    public void hilos(String id_usu, String id_joven, String estado){

        hiloconexionInsert = new ObtenerWebServiceInsert();
        hiloconexionInsert.execute(INSERT,"4",id_usu, id_joven, estado, id_congregacion);

    }

    private void temporizador(){

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private void updateList() {

        if (contador == 0){

            lista.setAdapter(adapter);


        }else if (contador == 1){

            lista.setAdapter(adapter);


        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;

    }

    public class ObtenerWebService extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                devuelve = "Cargando";

                                Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                titulo = alumnosJSON.getJSONObject(i).getString("nombres") + " " + alumnosJSON.getJSONObject(i).getString("apellidos");
                                descripcion = alumnosJSON.getJSONObject(i).getString("domicilio");
                                ruta = IP + alumnosJSON.getJSONObject(i).getString("foto");

                                m_id.add(alumnosJSON.getJSONObject(i).getString("id"));
                                //ruta = img2;

                                datos = new Datos(ruta, titulo, descripcion);
                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter = new AdapterDatoss(TotalJovenes.this, arraydatos);
                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {


            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerWebServiceValidar extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];
            final String valor = params[2];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            devuelve = "Ya marco asistencia el dia de hoy.";

                        }
                        else if (resultJSON.equals("2")) {

                            hilos(idd, valor, "s");
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {


            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public class ObtenerWebServiceInsert extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="4"){  //update

                try {
                    HttpURLConnection urlConn;

                    DataOutputStream printout;
                    DataInputStream input;
                    url = new URL(cadena);
                    urlConn = (HttpURLConnection) url.openConnection();
                    urlConn.setDoInput(true);
                    urlConn.setDoOutput(true);
                    urlConn.setUseCaches(false);
                    urlConn.setRequestProperty("Content-Type", "application/json");
                    urlConn.setRequestProperty("Accept", "application/json");
                    urlConn.connect();
                    //creo el objeto json
                    JSONObject jsonparam = new JSONObject();
                    jsonparam.put("id_usuario", params[2]);
                    jsonparam.put("id_joven", params[3]);
                    jsonparam.put("estado", params[4]);
                    jsonparam.put("id_congregacion", params[5]);
                    //envio los parametros post.
                    OutputStream os = urlConn.getOutputStream();
                    BufferedWriter write = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    write.write(jsonparam.toString());
                    write.flush();
                    write.close();

                    int respuesta = urlConn.getResponseCode();

                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                        while ((line = br.readLine()) != null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());

                        String resultJSON = respuestaJSON.getString("estado");

                        if (resultJSON == "1"){ //alumno actualizado correctamente
                            devuelve = "Asistencia marcada";
                            Log.d(devuelve,"resultado");
                        }else if(resultJSON == "2"){
                            devuelve = "Error al marcar la asistencia.";
                            Log.d(devuelve,"resultado");
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }


}
