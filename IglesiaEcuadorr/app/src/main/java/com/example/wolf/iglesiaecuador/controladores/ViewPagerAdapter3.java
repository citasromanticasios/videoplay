package com.example.wolf.iglesiaecuador.controladores;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wolf.iglesiaecuador.R;


/**
 * Created by reale on 13/07/2016.
 */
public class ViewPagerAdapter3 extends PagerAdapter {

        Activity activity;
        String[] images;
        String[] textos;
        LayoutInflater inflater;

        public ViewPagerAdapter3(Activity activity, String[] images, String[] textos) {
            this.activity = activity;
            this.images = images;
            this.textos = textos;

        }


        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            inflater = (LayoutInflater)activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.viewpager_item_nosotros,container,false);

            TextView titulo, contenido;

            titulo = (TextView) itemView.findViewById(R.id.lbl_titulo);
            contenido = (TextView) itemView.findViewById(R.id.lbl_contenido);

            titulo.setText(images[position]);
            contenido.setText(textos[position]);

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View)object);
        }
    }