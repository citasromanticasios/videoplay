package com.example.wolf.iglesiaecuador.controladores;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.wolf.iglesiaecuador.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Iglesias extends AppCompatActivity {

    String IP = "https://ieanjesusoficial.org/";
    String GET = IP + "web/ApiApp/api/iglesia/query_allIglesia.php";
    String GETID = IP + "web/ApiApp/api/iglesia/query_allIdIglesias.php";

    String img = "http://190.214.46.83/ieanjesus_general/";

    String img2 = "http://www.redcda.org/wp-content/uploads/2014/10/13984593091381272676iconoperfilazul.png";

    String mensaje = "";

    ArrayList<Datos> arraydatos = new ArrayList<Datos>();
    Datos datos;
    AdapterDatoss adapter;
    AdapterDatoss adapter2;

    ObtenerWebService hiloconexion;
    ObtenerWebServiceID hiloconexion2;

    String id;
    String titulo;
    String descripcion;
    String ruta;

    String bus = "";

    Integer contador = 0;
    Integer contador2 = 0;
    Integer tiempo = 1000;
    Integer tiempo2 = 1000;

    final ArrayList<String> latitud = new ArrayList<String>();
    final ArrayList<String> longitud = new ArrayList<String>();
    final ArrayList<String> pastor = new ArrayList<String>();
    final ArrayList<String> numero= new ArrayList<String>();
    final ArrayList<String> img_iglesia= new ArrayList<String>();
    final ArrayList<String> nombre_de_iglesia = new ArrayList<String>();
    final ArrayList<String> direccion = new ArrayList<String>();

    Double lat = 0.0, lon = 0.0;

    ArrayList<LatLng> locationsT = new ArrayList<LatLng>();
    ArrayList<String> nombre_iglesiaT = new ArrayList<String>();
    ArrayList<String> pastorT = new ArrayList<String>();

    ListView lista;
    EditText buscar;

    Intent siguiente;

    String Iglesia_, Pastor_, Direccion_, Telefono_, IMG_;


    ImageView Foto_Iglesia_;
    TextView igle, past, direc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iglesias);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lista = (ListView)findViewById(R.id.lista);
        buscar = (EditText)findViewById(R.id.txt_buscar);

        locationsT = (ArrayList<LatLng>) getIntent().getSerializableExtra("miLista");
        pastorT = (ArrayList<String>) getIntent().getSerializableExtra("miListaPastor");
        nombre_iglesiaT = (ArrayList<String>) getIntent().getSerializableExtra("miListaIglesia");

        if (isOnlineNet()){

            hiloconexion = new ObtenerWebService();
            hiloconexion.execute(GET,"1");
            temporizador();

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
        }

        Button fab = (Button) findViewById(R.id.btn_buscar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOnlineNet()){

                    bus = buscar.getText().toString();
                    Log.d(bus, " Boton");
                    hiloconexion2 = new ObtenerWebServiceID();
                    String cadena = GETID + "?texto=" + buscar.getText().toString();
                    Log.d(cadena, " Boton");
                    hiloconexion2.execute(cadena,"1");
                    tiempo = 1000;
                    contador = 0;
                    updateListID();

                }else{
                    mensaje("Hermano, lo sentimos no tiene acceso a internet.");
                }

                //lista.setAdapter(adapter2);

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                lat = Double.valueOf(latitud.get(position).toString());
                lon = Double.valueOf(longitud.get(position).toString());

                Direccion_ = direccion.get(position).toString();
                Pastor_ = pastor.get(position).toString();
                Telefono_ = numero.get(position).toString();
                IMG_ =IP + img_iglesia.get(position).toString();

                modal_votar();

            }
        });

    }

    public void temporizador(){

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();



    }

    private  void hilos(String search){

        hiloconexion2 = new ObtenerWebServiceID();
        String cadena = GETID + "?texto=" + search;
        hiloconexion2.execute(cadena,"2");

    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private void updateList() {

        if (contador == 0){

            lista.setAdapter(adapter);

        }else if (contador == 1){

            lista.setAdapter(adapter);

        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;

    }

    private void updateListID() {

        if (contador == 0){

            lista.setAdapter(adapter2);


        }else if (contador == 1){

            lista.setAdapter(adapter2);


        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;
        //lo aplico

    }

    public class ObtenerWebService extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombre_pastor"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("nombre_pastor"));

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                mensaje = "Cargando";

                                Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(i).getString("nombre_pastor"),"esto es lo q trajo el nombre");
                                Log.d(alumnosJSON.getJSONObject(i).getString("ciudad"),"esto es lo q trajo la ciudad");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                descripcion =  alumnosJSON.getJSONObject(i).getString("distrito") + " " + alumnosJSON.getJSONObject(i).getString("direccion") + " " + alumnosJSON.getJSONObject(i).getString("fecha_misionero") ;
                                titulo = alumnosJSON.getJSONObject(i).getString("nombre_pastor");
                                ruta = img + alumnosJSON.getJSONObject(i).getString("foto_pastor");
                                //ruta = img2;

                                latitud.add(alumnosJSON.getJSONObject(i).getString("latitud"));
                                longitud.add(alumnosJSON.getJSONObject(i).getString("longitud"));
                                pastor.add(alumnosJSON.getJSONObject(i).getString("nombre_pastor"));
                                numero.add(alumnosJSON.getJSONObject(i).getString("telefono"));
                                img_iglesia.add(alumnosJSON.getJSONObject(i).getString("panoramica_exterior"));


                                datos = new Datos(ruta, titulo, descripcion);
                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter = new AdapterDatoss(Iglesias.this, arraydatos);
                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {


            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerWebServiceID extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombre_pastor"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            arraydatos.clear();
                            latitud.clear();
                            longitud.clear();
                            pastor.clear();
                            numero.clear();
                            img_iglesia.clear();

                            for (int i = 0; i < alumnosJSON.length(); i++) {


                                devuelve = "Cargando Busqueda";

                                Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(i).getString("nombre_pastor"),"esto es lo q trajo el nombre");
                                Log.d(alumnosJSON.getJSONObject(i).getString("ciudad"),"esto es lo q trajo la ciudad");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                titulo = alumnosJSON.getJSONObject(i).getString("nombre_pastor");
                                descripcion = alumnosJSON.getJSONObject(i).getString("ciudad") + " " + alumnosJSON.getJSONObject(i).getString("distrito") + " " + alumnosJSON.getJSONObject(i).getString("direccion") + " " + alumnosJSON.getJSONObject(i).getString("fecha_misionero") ;
                                ruta = img + alumnosJSON.getJSONObject(i).getString("foto_pastor");
                                //ruta = img2;

                                latitud.add(alumnosJSON.getJSONObject(i).getString("latitud"));
                                longitud.add(alumnosJSON.getJSONObject(i).getString("longitud"));
                                pastor.add(alumnosJSON.getJSONObject(i).getString("nombre_pastor"));
                                numero.add(alumnosJSON.getJSONObject(i).getString("telefono"));
                                img_iglesia.add(alumnosJSON.getJSONObject(i).getString("panoramica_exterior"));

                                direccion.add(descripcion);

                                datos = new Datos(ruta, titulo, descripcion);
                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter2 = new AdapterDatoss(Iglesias.this, arraydatos);
                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "No se encontraron datos que coincidan con su busqueda";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void modal_votar(){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(Iglesias.this);
        View mView = getLayoutInflater().inflate(R.layout.modal_paquete, null);

        RelativeLayout mUbicacion = (RelativeLayout) mView.findViewById(R.id.rellay_timeline);
        RelativeLayout mLlamar = (RelativeLayout) mView.findViewById(R.id.rellay_friends);


        past = (TextView) mView.findViewById(R.id.lbl_pastor) ;
        direc = (TextView) mView.findViewById(R.id.lbl_direccion);

        Foto_Iglesia_ = (ImageView) mView.findViewById(R.id.img_iglesia);

        Glide.with(Iglesias.this).load(IMG_).into(Foto_Iglesia_);


        past.setText(Pastor_);
        direc.setText(Direccion_);

        //final EditText pass = (EditText) mView.findViewById(R.id.txt_clave);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        mUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(Iglesias.this, MapsActivity.class);

                siguiente.putExtra("busLat", lat.toString() );
                siguiente.putExtra("busLong", lon.toString() );

                siguiente.putExtra("miLista", locationsT);
                siguiente.putExtra("miListaPastor", pastorT);
                siguiente.putExtra("miListaIglesia", nombre_iglesiaT);

                siguiente.putExtra("leandro","1");

                startActivity(siguiente);


                dialog.dismiss();

            }
        });

        mLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(Intent.ACTION_DIAL);
                siguiente.setData(Uri.parse("tel:" + Telefono_));

                startActivity(siguiente);

                dialog.dismiss();

            }
        });


    }

}


