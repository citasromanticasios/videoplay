package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Notificaciones extends AppCompatActivity {

    Integer contador = 0;
    Integer tiempo = 3000;

    String IP = "https://ieanjesusoficial.org/";
    String GET = IP + "web/ApiApp/api/iglesia/query_allNoti.php";

    String img = "http://190.214.46.83/ieanjesus_general/";
    String img2 = "https://www.minminas.gov.co/documents/10180/621221/NotificacionesJudiciales.png/f6d6f9c1-5b98-42b5-9ee7-d718da088e8f?t=1424128788247";

    String mensaje = "";

    final ArrayList<String> m_id = new ArrayList<String>();
    final ArrayList<String> m_titulo = new ArrayList<String>();
    final ArrayList<String> m_descripcion = new ArrayList<String>();

    ArrayList<Datos> arraydatos = new ArrayList<Datos>();
    Datos datos;
    AdapterDatoss adapter;

    ObtenerWebService hiloconexion;

    String id;
    String titulo;
    String descripcion;
    String ruta;

    ListView lista;

    Intent siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lista = (ListView)findViewById(R.id.lista);

        if (isOnlineNet()){

            hilos();
            temporizador();

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(Notificaciones.this, MainActivity.class);
                startActivity(siguiente) ;

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String texto = String.valueOf(parent.getItemAtPosition(position));

                siguiente = new Intent(Notificaciones.this, Contenido.class);

                siguiente.putExtra("id", m_id.get(position).toString() );
                siguiente.putExtra("titulo", m_titulo.get(position).toString());
                siguiente.putExtra("descripcion", m_descripcion.get(position).toString() );

                startActivity(siguiente);

            }
        });

    }

    public void hilos(){

        hiloconexion = new ObtenerWebService();
        hiloconexion.execute(GET,"1");

    }

    public void temporizador(){
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();
    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private void updateList() {

        if (contador == 0){

            lista.setAdapter(adapter);

            mensaje("Cargando");

        }else if (contador == 1){

            lista.setAdapter(adapter);

            mensaje("Cargando");

        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;

    }

    public class ObtenerWebService extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                mensaje = "Cargando";

                                Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                titulo = alumnosJSON.getJSONObject(i).getString("nombres") + " " + alumnosJSON.getJSONObject(i).getString("apellidos");
                                descripcion = alumnosJSON.getJSONObject(i).getString("observacion");
                                ruta = img + alumnosJSON.getJSONObject(i).getString("foto");
                                //ruta = img2;

                                m_id.add(alumnosJSON.getJSONObject(i).getString("id"));
                                m_titulo.add(alumnosJSON.getJSONObject(i).getString("nombres") + " " + alumnosJSON.getJSONObject(i).getString("apellidos"));
                                m_descripcion.add(alumnosJSON.getJSONObject(i).getString("observacion"));

                                datos = new Datos(ruta, descripcion, titulo);
                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter = new AdapterDatoss(Notificaciones.this, arraydatos);
                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {
/*
            mensaje = s;

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();
*/
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}
