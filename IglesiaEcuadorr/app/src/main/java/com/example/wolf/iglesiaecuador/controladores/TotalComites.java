package com.example.wolf.iglesiaecuador.controladores;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class TotalComites extends AppCompatActivity {

    String IP = "https://ieanjesusoficial.org/";
    //String IP = "http://190.214.46.83/";

    String GET = IP + "web/ApiApp/api/iglesia/query_allComite.php?texto=";
    String GETID = IP + "web/ApiApp/api/iglesia/query_allComitesId.php";

    String img = "http://190.214.46.83/ieanjesus_general/";

    String img2 = "http://www.redcda.org/wp-content/uploads/2014/10/13984593091381272676iconoperfilazul.png";

    String mensaje = "";

    ArrayList<Datos> arraydatos = new ArrayList<Datos>();
    Datos datos;
    AdapterDatoss adapter;

    ObtenerWebService hiloconexion;
    ObtenerWebServiceID hiloconexion2;

    String id;
    String titulo;
    String descripcion;
    String ruta;

    Integer contador = 0;
    Integer tiempo = 3000;

    final ArrayList<String> m_id = new ArrayList<String>();
    final ArrayList<String> ruta2= new ArrayList<String>();
    final ArrayList<String> titulo2 = new ArrayList<String>();
    final ArrayList<String> descricion2= new ArrayList<String>();

    ListView lista;
    EditText buscar;

    String id_congregacion;

    String bus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_comites);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lista = (ListView)findViewById(R.id.lista);
        buscar = (EditText)findViewById(R.id.txt_buscar);

        SharedPreferences sharedPreferences = getSharedPreferences(Ingresar.globalPreferenceName, MODE_PRIVATE);

        id_congregacion = sharedPreferences.getString("id_congregacion","Id perdido");

        if (isOnlineNet()){

            hiloconexion = new ObtenerWebService();
            hiloconexion.execute(GET + id_congregacion,"1");
            temporizador();

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
        }

        Button fab = (Button) findViewById(R.id.btn_buscar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOnlineNet()){

                    bus = buscar.getText().toString();
                    Log.d(bus, " Boton");
                    hiloconexion2 = new ObtenerWebServiceID();
                    String cadena = GETID + "?texto=" + buscar.getText().toString();
                    Log.d(cadena, " Boton");
                    hiloconexion2.execute(cadena,"1");
                    tiempo = 3000;
                    contador = 0;
                    updateListID();

                }else{
                    mensaje("Hermano, lo sentimos no tiene acceso a internet.");
                }

            }
        });



    }

    public void temporizador(){

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private void updateList() {

        if (contador == 0){

            lista.setAdapter(adapter);


        }else if (contador == 1){

            lista.setAdapter(adapter);


        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;

    }

    public class ObtenerWebService extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                devuelve = "Cargando";

                                Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                titulo = alumnosJSON.getJSONObject(i).getString("nombres") + " " + alumnosJSON.getJSONObject(i).getString("apellidos");
                                descripcion = alumnosJSON.getJSONObject(i).getString("nombre_comite");
                                ruta = IP + alumnosJSON.getJSONObject(i).getString("foto");
                                //ruta = img2;

                                datos = new Datos(ruta, titulo, descripcion);
                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter = new AdapterDatoss(TotalComites.this, arraydatos);
                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {


            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public class ObtenerWebServiceID extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombres"),"esto es lo q trajo el estado f4era");

                            //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                            arraydatos.clear();

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                mensaje = "Cargando";

                                Log.d(mensaje,"esto es lo q trajo el estado");

                                Log.d(alumnosJSON.getJSONObject(i).getString("nombres"),"esto es lo q trajo el estado");

                                //mensaje(alumnosJSON.getJSONObject(0).getString("grupo"));

                                titulo = alumnosJSON.getJSONObject(i).getString("nombres") + " " + alumnosJSON.getJSONObject(i).getString("apellidos");
                                descripcion = alumnosJSON.getJSONObject(i).getString("domicilio");
                                ruta = img + alumnosJSON.getJSONObject(i).getString("foto");
                                //ruta = img2;

                                datos = new Datos(ruta, titulo, descripcion);

                                arraydatos.add(datos);

                                //creo el adater personalizado
                                adapter = new AdapterDatoss(TotalComites.this, arraydatos);
                            }

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private void updateListID() {

        if (contador == 0){

            lista.setAdapter(adapter);

            //mensaje("Cargando");

        }else if (contador == 1){

            lista.setAdapter(adapter);

            //mensaje("Cargando");

        }else if (contador == 2){

            tiempo = 10000000;

        }

        contador++;
        //lo aplico

    }

}
