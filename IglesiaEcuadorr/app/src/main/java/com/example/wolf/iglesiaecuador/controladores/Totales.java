package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.wolf.iglesiaecuador.R;


public class Totales extends AppCompatActivity {

    Button bautizados, bautizadosES, jovenes, comites, membrecias;

    Intent siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_totales);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bautizados = (Button)findViewById(R.id.btn_bautizados);
        bautizadosES = (Button)findViewById(R.id.btn_bautizados_es);
        comites = (Button)findViewById(R.id.btn_comites);
        jovenes = (Button)findViewById(R.id.btn_jovenes);
        membrecias = (Button)findViewById(R.id.btn_membrecias);

        bautizados.setText("Bautizados" + " " + getIntent().getStringExtra("bautizados"));
        bautizadosES.setText("Bautizados ES" + " " + getIntent().getStringExtra("es"));
        jovenes.setText("Jovenes" + " " + getIntent().getStringExtra("jovenes"));
        comites.setText("Comites" + " " + getIntent().getStringExtra("comites"));
        membrecias.setText("Membrecias" + " " + getIntent().getStringExtra("membrecia"));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        bautizados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(Totales.this, TotalBautizados.class);
                startActivity(siguiente);

            }
        });

        bautizadosES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(Totales.this, TotalBautizadosES.class);
                startActivity(siguiente);

            }
        });

        jovenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(Totales.this, TotalJovenes.class);
                startActivity(siguiente);

            }
        });

        comites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(Totales.this, TotalComites.class);
                startActivity(siguiente);

            }
        });

        membrecias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                siguiente = new Intent(Totales.this, TotalMembrecias.class);
                startActivity(siguiente);

            }
        });
    }

}
