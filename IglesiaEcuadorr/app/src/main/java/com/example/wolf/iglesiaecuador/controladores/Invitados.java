package com.example.wolf.iglesiaecuador.controladores;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

public class Invitados extends AppCompatActivity {
    //mapa

    String IPl = "https://ieanjesusoficial.org/";
    String GETl = IPl + "web/ApiApp/api/iglesia/query_allIglesia.php";
    
    ObtenerWebServicel hiloconexionl;

    final ArrayList<LatLng> locations = new ArrayList<LatLng>();

    final ArrayList<String> latitud = new ArrayList<String>();
    final ArrayList<String> longitud = new ArrayList<String>();
    final ArrayList<String> pastor = new ArrayList<String>();
    final ArrayList<String> nombre_iglesia = new ArrayList<String>();
    final ArrayList<String> numero= new ArrayList<String>();
    final ArrayList<String> img_iglesia= new ArrayList<String>();

    public class ObtenerWebServicel extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Log.d(alumnosJSON.getJSONObject(0).getString("nombre_pastor"),"esto es lo q trajo el estado f4era");

                            for (int i = 0; i < alumnosJSON.length(); i++) {

                                devuelve = "Cargando";

                                latitud.add(alumnosJSON.getJSONObject(i).getString("latitud"));
                                longitud.add(alumnosJSON.getJSONObject(i).getString("longitud"));

                                pastor.add(alumnosJSON.getJSONObject(i).getString("nombre_pastor"));
                                nombre_iglesia.add(alumnosJSON.getJSONObject(i).getString("nombre_iglesia"));
                                numero.add(alumnosJSON.getJSONObject(i).getString("telefono"));
                                img_iglesia.add(alumnosJSON.getJSONObject(i).getString("panoramica_exterior"));

                                Double lat = Double.parseDouble(alumnosJSON.getJSONObject(i).getString("latitud"));
                                Double lon = Double.parseDouble(alumnosJSON.getJSONObject(i).getString("longitud"));

                                LatLng conectar = new LatLng(lat , lon);
                                locations.add(conectar);

                                Log.d("Leandro", locations.get(i).toString());


                            }



                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Consulta fallida";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                }catch (IOException e) {
                    devuelve = e.toString();
                } catch (JSONException e) {
                    devuelve = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            mensaje(s);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    //login

    static final int REQUEST_IMAGE_CAPTURE = 1;



    String IP = "https://ieanjesusoficial.org";
    String GETID = IP + "/web/ApiApp/api/iglesia/query_login.php";

    ObtenerWebServiceID hiloconexion2;

    Button ingresar;
    Button facial;

    EditText email;
    EditText clave;

    String varEmail;
    String varClave;

    Integer seconds = 0;

    public static String globalPreferenceName = "com.haangnd.profile";

    //---------------

    ViewPager viewPagerInstrutores, viewPagerAreas;
    ViewPagerAdapter2 adapterView;
    ViewPagerAdapter3 adapterView2;

    private String[] imagesInstructores = {

            "https://facebook.com",
            "http://Twitter.com",
            "https://youtube.com",
            "https://soundcloud.com",
            "http://ieanjesusoficial.org/login.php"

    };

    private String[] imagesAreasNosotros = {

            "Quienes Somos",
            "Vision",
            "Mision"
    };

    private String[] imagesPaquetes = {

            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0InexA_daGz-kGqjmta3NCEYaxwdb2Fok85GPDveFC2befVi9rw",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAWDg4EIunu2DAxxva35o5eLG-nAGZ-vo3Pa82T44m7MRzH2kN"

    };

    private String[] textInstructores = {

            "https://www.youtube.com/user/IeanJesus1?feature=sub_widget_1",
            "https://www.facebook.com/ieanjesusoficial/",
            "https://twitter.com/ieanjesusof",
            "https://soundcloud.com/ieanjesusof",
            "http://ieanjesusoficial.org/login.php"


    };

    private String[] textAreasNosotros = {

            "La Iglesia Evangélica Apostólica del Nombre de Jesús es la organización religiosa no-católica más grande de Ecuador. Su inicio es a partir del año 1958, y siendo una obra misionera extranjera de la Iglesia Pentecostal Unida de Colombia, ahora cuenta con más de 875 congregaciones y una membresía aproximada de 115,000 miembros en Ecuador.  También está predicando el evangelio en 12 países, bajo la coordinación del pastor director de misiones extranjeras en Ecuador.  Los países a los cuales la misión ecuatoriana ha llegado son: Perú, Chile, Uruguay, Venezuela, San Salvador, España, Italia, Suiza, Costa Rica, Alemania. Dos países más están en la visión de obra extranjera como son: Israel y Brasill etc.  La actividad Evangelística y Misionera de la Iglesia tiene su sustento y apoyo en las sagradas escrituras inspiradas por el mismo Dios. La misma que se ha constituido en un instrumento orientador y de referencia para todo el quehacer de amor al prójimo.  En esta línea, la Iglesia Evangélica Apostólica del nombre de Jesús, se ha enmarcado en los siguientes principios rectores para elaborar sus planes de acción:  Art. 1  Por cuanto la voluntad de Dios es sacar del mundo un pueblo salvo para la gloria de su nombre, pueblo que constituye la Iglesia de Jesucristo, la cual debe estar edificada sobre el fundamento de los Apóstoles y profetas siendo la principal piedra del ángulo Jesucristo mismo, Ef 2:20  En base a una permanente comunión entre miembros de la iglesia, impartir consejos y ser instruidos en la palabra de Dios para la obra del ministerio, y para el ejercicio de los oficios espirituales previstos en la Santa Biblia.  Esta comunión establecida por Dios es sostenida por el Espíritu Santo. Y El es el que ha dado a su Iglesia, Apóstoles, Profetas, Evangelistas Pastores y Maestros. Ef. 4:11,  Art. 2  Declaramos y nos esforzamos por guardar la unidad del Espíritu en el versículo de la paz; un cuerpo y un espíritu como fuiste también llamados en una misma esperanza de vuestra vocación; un Señor, una Fe, un Bautismo, un Dios y Padre de todos, el cual es sobre todos y por todos y en todos, Ef. 4:3-6  Art. 3   La Iglesia tendrá los siguientes propósitos:  1. Establecer la obra sobre una base eficiente en enseñanza, conducta y métodos bíblicos.  2. Incrementar la confraternidad y comunión de los que profesan la misma fe, y atraer a los de fe semejante.  3. Promover una mayor evangelización del mundo, procurar incrementarla en territorios no evangelizados y cuidar las congregaciones ya existentes; enseñando y predicando la Sana Doctrina, siguiendo las prácticas de la Iglesia del Señor establecidas en el libro de los Hechos de los Apóstoles.  4. Apoyar económicamente dentro de sus posibilidades a personas o entidades, en programas sociales, filantrópicos, educativos, de salud o caritativos en la medida que estos propendan para alcanzar los fines expresados en los estatutos.",
            "HASTA QUE EL ECUADOR SEA LLENO DEL EVANGELIO  En un lapso de 4 años la IEANJESUS mantendrá y fortalecerá el crecimiento a nivel espiritual, administrativo y financiero, tanto en la membresía de ministros como en la de feligreses, consolidando más nuestra organización a nivel nacional e internacional.",
            "Una de las metas principales de las administraciones de turno, es extender la Obra del Señor dentro y fuera de nuestras fronteras. Para esto se aplica políticas de gobiernos que están orientadas a la extensión de la obra en los distritos y en las diferentes naciones donde se tenga la visión de llevar el mensaje a todas las almas. Dando énfasis y focalizando sus esfuerzos sobre todos en los estratos sociales más vulnerables de la sociedad.  Sin lugar a dudas la disponibilidad de recursos humanos como económicos, es útil para cumplir con las metas establecidas por la Iglesia como misión principal, así como también las oportunas bases de datos de información de casi cuatro años sirven para realizar un toma de decisiones más oportunas,  En la actualidad la Iglesia Evangélica apostólica del nombre de Jesús, ha mirado otros aspectos como la educación y el desarrollo social de la sociedad, perfeccionándose cada vez más y mejorando sus habilidades para contribuir con su misión a mejorar los estratos sociales más vulnerables utilizando procedimientos metodológicos tecnificados para brindar una mejor asistencia a los que deseen servirse de ella.  Con indicadores de gestión a través de procesos y estadísticas confiables para atender a una comunidad que cada día mejora su calidad de vida y que a la vez contribuye a mantener una mejor sociedad.  MARCO FILOSÓFICO ORGANIZACIONAL  6.1 Valores Corporativos.- Los valores corporativos son el alma de la cultura organizacional, son los que revitalizan y la ponen operativa y nos llevan de la misión a la realización de la visión. De esta manera, la actividad institucional se rige bajo los siguientes valores:                 AMOR                        CONVICCIÓN  Amor.- Este valor es el que permite a la organización recibir a los que vienen a Cristo sin hacer acepción de personas “Y ahora permanecen la fe, la esperanza y el amor, estos tres; pero el mayor de ellos es el amor.”  Convicción.- Este valor permite a cada sirvo de Cristo que es un miembro de la organización a mantener sus principios y perseverar hasta el fin sin tener otro propósito que el de su salvación y a ser instrumentos para que otros sean salvos. “Se fiel hasta la muerte y yo te daré la corona de vida”  La misión de la Iglesia Evangélica Apostólica del nombre de Jesús es cumplir con el mandamiento dispuesto por Dios en las sagradas escrituras “Y les dijo id por todo el mundo y predicad el evangelio a toda criatura.” San Marcos 16:15"

    };

    private String[] textPaquetes = {

            "Individual",
            "Equipo"

    };

    private LinearLayout layout_instructores, layout_paquetes, layout_areas, layout_contactanos, layout_testimonios;

    EditText txt_nombre, txt_telefono, txt_email, txt_apellido, txt_pass;

    ImageView img_perfil;

    Intent siguiente;

    Integer countRadio = 0;

    private String STREAM_URL ="http://62.210.141.243:8151/stream/1/";
    private MediaPlayer mPlayer;

    ImageView deslizar;

    ImageView tomar;

    public static final int MY_PERMISSIONS_REQUEST_CAMARA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitados);

        modal_votar();

        mensaje("Deslizar el dedo de derecha a izquierda para cambiar de red.");

        //Login

        ingresar = (Button)findViewById(R.id.btn_ingresar);

        email = (EditText)findViewById(R.id.et_email);
        clave = (EditText)findViewById(R.id.et_clave);

        tomar = findViewById(R.id.img_foto);

        ingresar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                login();

//                if (Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1) {// Marshmallow+
//                    if (ContextCompat.checkSelfPermission(Invitados.this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) {
//                        // Should we show an explanation?
//                        if (ActivityCompat.shouldShowRequestPermissionRationale(Invitados.this, Manifest.permission.CAMERA)) {
//                            // Show an expanation to the user *asynchronously* -- don't block
//                            // this thread waiting for the user's response! After the user
//                            // sees the explanation, try again to request the permission.
//                        } else {
//                            // No se necesita dar una explicación al usuario, sólo pedimos el permiso.
//                            ActivityCompat.requestPermissions(Invitados.this,new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMARA );
//                            // MY_PERMISSIONS_REQUEST_CAMARA es una constante definida en la app. El método callback obtiene el resultado de la petición.
//                        }
//                    }else{ //have permissions
//                        abrirCamara ();
//                    }
//                }else{ // Pre-Marshmallow
//                    abrirCamara ();
//                }

            }
        });

        TextView ingre = (TextView)findViewById(R.id.lbl_ingresar);

        ingre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent siguiente = new Intent(Invitados.this, Ingresar.class);

                startActivity(siguiente);

            }
        });

        hiloconexionl = new ObtenerWebServicel();
        hiloconexionl.execute(GETl,"1");

        Calendar c = Calendar.getInstance();
        seconds = c.get(Calendar.MINUTE);

        //mensaje(String.valueOf(seconds));

        mPlayer = new MediaPlayer();

        layout_instructores = (LinearLayout)findViewById(R.id.layout_instructores);
        layout_paquetes = (LinearLayout)findViewById(R.id.layout_paquetes);
        layout_contactanos = (LinearLayout)findViewById(R.id.layout_contactanos);
        layout_testimonios = (LinearLayout)findViewById(R.id.layout_testimonios);
        layout_areas = (LinearLayout)findViewById(R.id.layout_areas);

        //Contenedor de perfil - Objetos

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //Datos google acceso

        Button menu = (Button) findViewById(R.id.boton_menu);

        WebView web = (WebView)findViewById(R.id.web);

        WebSettings webSettings = web.getSettings();
        webSettings.setJavaScriptEnabled(true);
        web.setWebViewClient(new WebViewClient());
        web.loadUrl("https://livestream.com/accounts/7677592/events/4374150");

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent siguiente = new Intent(Invitados.this, Ingresar.class);

                startActivity(siguiente);

            }
        });

        //Redes sociales
        viewPagerInstrutores = (ViewPager) findViewById(R.id.viewPagerInstructores);
        adapterView = new ViewPagerAdapter2(Invitados.this, imagesInstructores, textInstructores);
        viewPagerInstrutores.setAdapter(adapterView);

        //Nosotros
        viewPagerAreas = (ViewPager) findViewById(R.id.viewPagerAreas);
        adapterView2 = new ViewPagerAdapter3(Invitados.this, imagesAreasNosotros, textAreasNosotros);
        viewPagerAreas.setAdapter(adapterView2);

    }

    private void abrirCamara() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMARA : {
                // Si la petición es cancelada, el array resultante estará vacío.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // El permiso ha sido concedido.
                    abrirCamara ();
                } else {
                    // Permiso denegado, deshabilita la funcionalidad que depende de este permiso.
                }
                return;
            }
            // otros bloques de 'case' para controlar otros permisos de la aplicación
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            tomar.setImageBitmap(imageBitmap);
        }
    }

    public void login(){

        varEmail = email.getText().toString();
        varClave = clave.getText().toString();

        hiloconexion2 = new ObtenerWebServiceID();
        String cadena = GETID + "?telefono=" + varEmail + "&clave=" + varClave;

        hiloconexion2.execute(cadena,"1");

    }

    public class ObtenerWebServiceID extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        JSONObject respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        String resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d(resultJSON,"esto es lo q trajo el estado");

                        if (resultJSON.equals("1")){ //hay alumnos a mostrar

                            JSONArray alumnosJSON = respuestaJSON.getJSONArray("alumno"); //estado es el nombre del campo en el JSON

                            //Log.d(alumnosJSON.getJSONObject(0).getString("nombre_iglesia"),"esto es lo q trajo la parte de afuera de la iglesia");

                            Intent siguiente = new Intent(Invitados.this, MainActivity.class);

                            SharedPreferences.Editor editor = getSharedPreferences(globalPreferenceName, MODE_PRIVATE).edit();

                            editor.putString("id",alumnosJSON.getJSONObject(0).getString("id"));
                            editor.putString("id_perfil",alumnosJSON.getJSONObject(0).getString("id_perfil"));
                            editor.putString("id_congregacion",alumnosJSON.getJSONObject(0).getString("id_congregacion"));
                            editor.putString("foto", IP + alumnosJSON.getJSONObject(0).getString("foto_usuario"));
                            editor.putString("nombre",alumnosJSON.getJSONObject(0).getString("nombre_completo"));
                            editor.putString("not",String.valueOf(seconds));

                            editor.commit();

                            startActivity(siguiente);

                        }
                        else if (resultJSON.equals("2")){

                            devuelve = "Usuario o clave incorrectos";
                        }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d(devuelve,"esto es lo q trajo el estado");
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            s, Toast.LENGTH_SHORT);

            toast1.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            switch (item.getItemId()) {

                case R.id.n_redes:

                    //fragmentTransaction.replace(R.id.content, new FragmentInstructores()).commit();
                    mostrarInstructores(layout_instructores);

                    ocultarAreas(layout_areas);
                    ocultarPaquetes(layout_paquetes);
                    ocultarContactanos(layout_contactanos);
                    ocultarTestimonios(layout_testimonios);

                    return true;
                case R.id.n_nosotros:

                    mostrarAreas(layout_areas);

                    ocultarInstructores(layout_instructores);
                    ocultarPaquetes(layout_paquetes);
                    ocultarContactanos(layout_contactanos);
                    ocultarTestimonios(layout_testimonios);

                    return true;

                case R.id.n_iglesias:

                    siguiente = new Intent(Invitados.this, MapsActivity.class);
                    siguiente.putExtra("miLista", locations);
                    siguiente.putExtra("miListaPastor", pastor);
                    siguiente.putExtra("miListaIglesia", nombre_iglesia);
                    siguiente.putExtra("leandro","0");
                    startActivity(siguiente);

                    return true;

                case R.id.n_radio:

                    mostrarPaquetes(layout_paquetes);

                    ocultarInstructores(layout_instructores);
                    ocultarAreas(layout_areas);
                    ocultarContactanos(layout_contactanos);
                    ocultarTestimonios(layout_testimonios);

                    return true;

                case R.id.n_login:

                    mostrarContactanos(layout_contactanos);

                    ocultarInstructores(layout_instructores);
                    ocultarAreas(layout_areas);
                    ocultarPaquetes(layout_paquetes);
                    ocultarTestimonios(layout_testimonios);

                    return true;

            }
            return false;
        }

    };

    private void mensaje(String mensaje) {

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();


    }

    public void mostrarInstructores(View button)
    {
        if (layout_instructores.getVisibility() == View.GONE)
        {
            animarInstructores(true);
            layout_instructores.setVisibility(View.VISIBLE);
        }
    }

    public void ocultarInstructores(View button)
    {
        if (layout_instructores.getVisibility() == View.VISIBLE)
        {
            animarInstructores(false);
            layout_instructores.setVisibility(View.GONE);
        }

    }

    private void animarInstructores(boolean mostrar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.1f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);

        layout_instructores.setLayoutAnimation(controller);
        layout_instructores.startAnimation(animation);
    }

    //Paquetes

    public void mostrarPaquetes(View button)
    {
        if (layout_paquetes.getVisibility() == View.GONE)
        {
            animarPaquetes(true);
            layout_paquetes.setVisibility(View.VISIBLE);
        }
    }

    public void ocultarPaquetes(View button)
    {
        if (layout_paquetes.getVisibility() == View.VISIBLE)
        {
            animarPaquetes(false);
            layout_paquetes.setVisibility(View.GONE);
        }

    }

    private void animarPaquetes(boolean mostrar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.1f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);

        layout_paquetes.setLayoutAnimation(controller);
        layout_paquetes.startAnimation(animation);
    }

    //Contactanos

    public void mostrarContactanos(View button)
    {
        if (layout_contactanos.getVisibility() == View.GONE)
        {
            animarContactanos(true);
            layout_contactanos.setVisibility(View.VISIBLE);
        }
    }

    public void ocultarContactanos(View button)
    {
        if (layout_contactanos.getVisibility() == View.VISIBLE)
        {
            animarContactanos(false);
            layout_contactanos.setVisibility(View.GONE);
        }

    }

    private void animarContactanos(boolean mostrar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.1f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);

        layout_contactanos.setLayoutAnimation(controller);
        layout_contactanos.startAnimation(animation);
    }

    //Testimonios

    public void mostrarTestimonios(View button)
    {
        if (layout_testimonios.getVisibility() == View.GONE)
        {
            animarTestimonios(true);
            layout_testimonios.setVisibility(View.VISIBLE);
        }
    }

    public void ocultarTestimonios(View button)
    {
        if (layout_testimonios.getVisibility() == View.VISIBLE)
        {
            animarTestimonios(false);
            layout_testimonios.setVisibility(View.GONE);
        }

    }

    private void animarTestimonios(boolean mostrar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.1f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);

        layout_testimonios.setLayoutAnimation(controller);
        layout_testimonios.startAnimation(animation);
    }

    //Areas

    public void mostrarAreas(View button)
    {
        if (layout_areas.getVisibility() == View.GONE)
        {
            animarAreas(true);
            layout_areas.setVisibility(View.VISIBLE);
        }
    }

    public void ocultarAreas(View button)
    {
        if (layout_areas.getVisibility() == View.VISIBLE)
        {
            animarAreas(false);
            layout_areas.setVisibility(View.GONE);
        }

    }

    private void animarAreas(boolean mostrar)
    {
        AnimationSet set = new AnimationSet(true);
        Animation animation = null;
        if (mostrar)
        {
            //desde la esquina inferior derecha a la superior izquierda
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        }
        else
        {    //desde la esquina superior izquierda a la esquina inferior derecha
            animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 1.0f, Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.1f);
        }
        //duración en milisegundos
        animation.setDuration(500);
        set.addAnimation(animation);
        LayoutAnimationController controller = new LayoutAnimationController(set, 0.25f);

        layout_areas.setLayoutAnimation(controller);
        layout_areas.startAnimation(animation);
    }

    public void radio(){

        if (isOnlineNet()){

            if (countRadio == 0){

                try{
                    mPlayer.reset();
                    mPlayer.setDataSource(STREAM_URL);
                    mPlayer.prepareAsync();

                    mPlayer.setOnPreparedListener(new MediaPlayer.
                            OnPreparedListener(){
                        @Override
                        public void onPrepared(MediaPlayer mp){
                            mp.start();
                            countRadio = 1;

                        }
                    });

                } catch (IOException e){
                    e.printStackTrace();

                }

            }else{

                mPlayer.stop();
                countRadio = 0;
            }

        }else{
            mensaje("Hermano, lo sentimos no tiene acceso a internet.");
        }

    }

    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void modal_votar(){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(Invitados.this);
        View mView = getLayoutInflater().inflate(R.layout.modal_deslizar, null);

        final ImageView deslizart = (ImageView)mView.findViewById(R.id.img_indicaciones) ;


        //final EditText pass = (EditText) mView.findViewById(R.id.txt_clave);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        deslizart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                dialog.dismiss();

            }
        });




    }

}
