package com.example.wolf.iglesiaecuador.controladores;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.wolf.iglesiaecuador.R;


public class Slash extends AppCompatActivity {

    Integer tiempo = 3000;

    Integer contador = 0;

    Intent siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slash);

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(tiempo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                updateList();

                            }
                        });
                    }
                } catch (InterruptedException e) {

                    mensaje(e.toString());

                }
            }
        };

        t.start();

    }

    private void mensaje(String mensaje){

        Toast toast1 =
                Toast.makeText(getApplicationContext(),
                        mensaje, Toast.LENGTH_SHORT);

        toast1.show();

    }

    private void updateList() {

        if (contador == 0){

            mensaje("Jesus te bendiga");
            siguiente = new Intent (Slash.this, Invitados.class);
            startActivity(siguiente) ;

        }else{

            tiempo = 10000000;

        }

        Log.d(contador.toString(),"El contador");

        contador++;


    }
}
